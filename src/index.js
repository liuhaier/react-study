import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import Lists from './temp/List';
// import Study from './temp/Study';
// import Form from './temp/Form';
// import Comment from './temp/Comment';
// import Memo from './temp/Memo';
// import ZuJianTongXun from './temp/ZuJianTongXun';
// import TongXunContext from './temp/TongXunContext';
// import PropsTypeRule from './temp/PropsTypeRule';
// import DaDouDou from './temp/DaDouDou';
// import HookHandler from './temp/HookHandler';
// import HookCustom from './temp/HookCustom';
// import RouterHandler from './temp/RouterHandler';
// import ReduxStudy from "./temp/ReduxStudy";
// import {Provider} from 'react-redux'
// import store from "./store";
import Router from "./router";
// ReactDOM.render(
//     // 严格模式
//     <React.StrictMode>
//         <div>
//             {/*<Lists onFunction={() => console.log(11111)}/>*/}
//             {/*<Study/>*/}
//             {/*<Form/>*/}
//             {/* <Comment/> */}
//             <Memo/>
//             {/*<Comment/>*/}
//             {/*<ReduxStudy/>*/}
//             {/*<ZuJianTongXun/>*/}
//             {/*<TongXunContext/>*/}
//             {/*<PropsTypeRule*/}
//             {/*    name={"aaaaa"}*/}
//             {/*    tag={<span style={{color:'red'}}>element啊啊啊</span>}*/}
//             {/*    colors={['red','blue']}*/}
//             {/*    getColorsCallback={() => {console.log('组件的额啊啊啊啊啊啊')}}*/}
//             {/*    filter={{*/}
//             {/*    area:'dsadasdas',*/}
//             {/*    price: 111*/}
//             {/*}}/>*/}
//             {/*<DaDouDou />*/}
//             {/*<HookHandler />*/}
//             {/*<HookCustom />*/}
//             {/*<RouterHandler />*/}
//         </div>
//     </React.StrictMode>,
//   document.getElementById('root')
// );
// ReactDOM.render(
//     <Provider store={store}>
//         <ReduxStudy/>
//     </Provider>,
//   document.getElementById('root')
// );
ReactDOM.render( 
  <Router /> ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals