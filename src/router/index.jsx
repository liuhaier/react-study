import { BrowserRouter, Routes, Route } from "react-router-dom"



import RouterStudy from "../temp/RouterStudy"

import Error from "../pages/404"
import Home from "../pages/Home"
import Lists from "../pages/Lists"
import Detail from "../pages/Detail"
const BaseRouter = () => (
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<RouterStudy />}>
        <Route path="/home" element={<Home />}></Route>
        <Route path="/lists/:cid" element={<Lists />}></Route>
        <Route path="/detail" element={<Detail />}></Route>
      </Route>
      <Route path="*" element={<Error />}></Route>
    </Routes>
  </BrowserRouter>
)

export default BaseRouter;