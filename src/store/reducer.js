const defaultState = {
    num: 1,
    reduxString: 'redux值'
}


export default (state = defaultState, action) => {
    let newState = JSON.parse(JSON.stringify(state)) // 必须深拷贝不然改变不了
    switch (action.type) {
        case 'addNum':
            newState.num++;
            break;
        case 'editString':
            // action传值
            newState.reduxString = action.value;
            break;
    }
    return newState
}
