import { connect } from 'react-redux'
let random = Math.floor(Math.random() * 10) + 1

const divCss = {
    fontSize:'50px',
    color: `#40${random}`
}

const ReduxStudy = (props) => {
    return (
        <>
            <div style={divCss}>{props.num}</div>
            <div style={divCss}>{props.reduxString}</div>
            <button onClick={() => props.addNum()}>累加Num</button>
            <button onClick={() => props.editString()}>修改字符串</button>
        </>
    )
}
// 获取值
const mapStateToProps = (state) => {
    return state
}
// 操作值
const mapDispatchToProps = (dispatch) => {

    return {
        addNum() {
            const action = {type: 'addNum'}
            dispatch(action)
        },
        editString() {
            // action传值
            const action = {type: 'editString', value: random.toString()}
            dispatch(action)
        }
    }
}
// connect(state映射，dispatch映射)(组件名)
const reduxRes = connect(mapStateToProps,mapDispatchToProps)(ReduxStudy)

export default reduxRes
