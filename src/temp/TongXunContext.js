import React from "react";

/**
 * Provider、Consumer 就是处理多层组件传值的问题，解决嵌套多层组件传值才能到父组件里面
 * Provider提供数据
 * Consumer消费数据
 */

// 创建context得到两个组件
const { Provider, Consumer } = React.createContext();
class ContextTX extends React.Component {

    state = {
        msg: '11sdadasdasdas'
    }

    render() {
        return (
            <Provider value={this.state.msg}>
                <div>
                    <Node>
                        <h1>插槽</h1>
                    </Node>
                </div>
            </Provider>

        )
    }
}

const Node = props => {
    return (
        <div className="node">
            插槽的内容：{props.children}
            <Node1/>
        </div>
    )
}
const Node1 = () => {
    return (
        <div className="node1">
            <Node2 />
        </div>
    )
}
const Node2 = () => {
    return (
        <div className="node2">
            <Consumer>{data => <span>底层子组件 -- {data}</span>}</Consumer>
        </div>
    )
}

export default ContextTX
