import { Outlet, Link, useLocation, useNavigate } from "react-router-dom"
const RouterStudy = () => {

  const location = useLocation()
  console.log(location.pathname) // 当前页面路径名称


  const target = useNavigate()
  const gotoDtail = () => {
    target('/detail', {
      state: {
        goodsNo: '7712'
      }
    })
  }
  return (
    <>
      <button onClick={gotoDtail}>跳转到详情页</button>
      <hr />
      <ul>
        <li><Link to="/home">home页面</Link></li>
        <li><Link to="/lists/13">list页面</Link></li>
        <li><Link to="/detail?id=100">detail页面</Link></li>
      </ul>
      <hr />
      <Outlet />
    </>
  )
}

export default RouterStudy;