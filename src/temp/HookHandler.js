import React, {useState, useEffect} from "react";

import img from "../img/1.png"

function Example() {
    const [count, setCount] = useState(0);
    const [xy, setXy] = useState({
        x: 0,
        y: 0
    });

    useEffect(() => {
        function handleMousemove(e) {
            setXy({
                x: e.clientX,
                y: e.clientY,
            })
        }

        /**
         * 初始化的时候先执行
         */
        window.addEventListener('mousemove', handleMousemove)

        /**
         * 更新的时候这个先执行
         */
        return function cleanup() {
            window.removeEventListener('mousemove', handleMousemove)
        }
    }, [count]);


    function setCountBtn() {
        setCount(count + 1)
    }

    return (
        <div>
            <p>{count}</p>
            <p>
                {xy.x}，{xy.y}
            </p>
            {/*<img src={img} style={{*/}
            {/*    width: '64px',*/}
            {/*    height: '64px',*/}
            {/*    position: 'absolute',*/}
            {/*    top: xy.y - 34,*/}
            {/*    left: xy.x - 24*/}
            {/*}}/>*/}
            <button onClick={() => setCountBtn()}>点击我</button>
        </div>
    )
}


export default Example
