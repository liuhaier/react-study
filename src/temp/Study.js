import React from "react";

// this点击写法
function handleClick(e) {
    console.log('点击我了饿')
    return e.preventDefault()
}

function Path() {
    return (
        <a href="//pzds.com" onClick={handleClick}>点击我</a>
    )
}

class Study extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            text: 'aa',
            city: 'xg'
        }
        /**
         * 方法三
         * 使用 bind方法
         */
        // this.setCount = this.setCount.bind(this)
    }

    // state = {
    //     count: 0
    // }

    /**
     * 方法二
     * class实例
     */
    setCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    // 实现双向绑定
    setValue = e => {
        this.setState({
            text: e.target.value
        })
    }
    // 实现双向绑定
    setCity = e => {
        this.setState({
            city: e.target.value
        })
    }
    /**
     * 方法一
     * 内嵌箭头函数
     */
    // setCount () {
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }

    render() {
        return (
            <div>
                {/*<h1 onClick={() => this.setCount()}>11111111{this.state.count}</h1>*/}
                <input type="text" value={this.state.text} onChange={this.setValue}/>
                <select value={this.state.city} onChange={this.setCity}>
                    <option value="bj">北京</option>
                    <option value="sh">上海</option>
                    <option value="xg">香港</option>
                </select>
                <h1 onClick={this.setCount}>{this.state.count}</h1>
                <Path />
            </div>
        )
    }
}

export default Study;
