import React from "react";

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            city: 'sh',
            isCheck: true
        }
    }

    handlerForm = e => {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }
    render() {
        return (
            <div>
                <form>
                    <label>名字：
                        <input onChange={this.handlerForm} name="name" type="text" value={this.state.name}/>
                    </label>
                    <label>下拉框：
                        <select onChange={this.handlerForm} name="city" value={this.state.city}>
                            <option value="bj">北京</option>
                            <option value="sh">上海</option>
                            <option value="xg">香港</option>
                        </select>
                    </label>
                    <label>选择框：
                        <input onChange={this.handlerForm} name="isCheck" type="checkbox" checked={this.state.isCheck}/>
                    </label>
                </form>
            </div>
        )
    }
}

export default Form;
