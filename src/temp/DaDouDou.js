import React from "react";

class DaDouDou extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
    }

    addCount = (resurrection) => {
        let count = resurrection ? 0 : this.state.count + 1
        this.setState({
            count: count
        })
    }

    render() {
        return (
            <div>
                {
                    this.state.count > 3 ? <p>豆豆被打死了~</p> : <Counter count={this.state.count}/>
                }
                <button onClick={() => this.addCount()}>点击</button>
                {/*向方法传参数*/}
                <button onClick={() => this.addCount(true)}>复活</button>
            </div>
        )
    }
}

class Counter extends React.Component{

    constructor(props) {
        super(props);
    }

    componentWillUnmount() {
        console.log(`组件销毁了`)
    }

    render() {
        return (
            <div>打豆豆的次数：{this.props.count}</div>
        )
    }
}

export default DaDouDou
