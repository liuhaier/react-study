import React from "react";



class Children extends React.Component {

    state = {
        msg: '刷丢眼2222222222'
    }

    emitValue = () => {
        this.props.getMsg(this.state.msg)
    }

    render() {
        return (
            <div>
                <span>父组件传进来的：{this.props.title}</span>
                <button onClick={this.emitValue}>点击我，把值传给父组件</button>
            </div>
        )
    }
}

const Xd1 = props => {
    return <div>{props.count}</div>
}
const Xd2 = props => {
    return <button onClick={() => props.setCountByFather()}>+1</button>
}

class ZuJianTongXun extends React.Component {

    state = {
        resMsg: '',
        count: 0
    }

    setCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    getChildrenMsg = res => {
        this.setState({
            resMsg: res
        })
        console.log(res, `=========>`)
    }

    render() {
        return (
            <div>
                <Children title='1213112' getMsg={this.getChildrenMsg}/>
                <span>我是父组件：{this.state.resMsg}</span>
                <hr/>
                这里是兄弟组件传值的方法：
                <Xd1 count={this.state.count}/>
                <Xd2 setCountByFather={this.setCount}/>
            </div>
        )
    }
}

export default ZuJianTongXun;
