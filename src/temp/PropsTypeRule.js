import React from 'react';

/**
 * 开启props校验
 */
import PropTypes from "prop-types";

class PropsTypeRule extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getColorsCallback()
    }

    render() {
        const arr = this.props.colors;
        const lis = arr.map((item,index) => <li key={index}>{item}</li>)
        return (
            <div>
                <ul>传入的数组：{lis}</ul>
                <div>字符串：{this.props.name}</div>
                <div>这是props默认值：{this.props.pageSize}</div>
                <div>元素标签：{this.props.tag}</div>
            </div>
        )
    }
}

/**
 * 设置类型
 * 组件名+propTypes
 */
PropsTypeRule.propTypes = {
    pageSize: PropTypes.number,
    colors: PropTypes.array.isRequired, // 数组必填
    name: PropTypes.string,
    getColorsCallback: PropTypes.func, // 方法
    tag: PropTypes.element, // dom元素
    // 可以指定一个对象由特定的类型值组成
    filter: PropTypes.shape({
        area: PropTypes.string,
        price: PropTypes.number
    }),
}

/**
 * 设置默认值
 */
PropsTypeRule.defaultProps = {
    pageSize: 10
}
export default PropsTypeRule
