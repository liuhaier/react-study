import React, { useState, memo } from 'react'

const h1Css = {
  color: "red"
}

// const Child = () => {
//   console.log('11')
//   return <div>子组件</div>
// }


// memo就是“记忆”memory的简写，防止父组件每次的点击事件都会触发子组件
const Child = memo(() => {
  console.log('11')
  return <div>子组件</div>
})

function Memo() {

  const [num,setNum] = useState(1)

  return (
    <>
      <h1 style={h1Css}>{num}</h1>
      <button onClick={() => setNum( num + 1 )}>点击累加num</button>
      <Child/>
    </>
  )
}

export default Memo;