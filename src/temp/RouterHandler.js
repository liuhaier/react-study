import React from 'react';
import { BrowserRouter as Router, Route, Link, Routes, withRouter } from 'react-router-dom';

const Home = () => <div>这是首页页面</div>;
const Login = () => {

    function back() {
        window.history.go(-1)
    }

    return (
        <div>
            这是登录页面
            <button onClick={back}>返回</button>
        </div>
    )
};

export default function RouterHandler() {
    return (
        <Router>
            <div>
                <h1>路由基础</h1>
                <ul>
                    <li>
                        <Link to='/'>- 首页</Link>
                    </li>
                    <li>
                        <Link to='/login'>- 登陆页面</Link>
                    </li>
                </ul>
                <Routes>
                    <Route exact path='/' element={<Home />} />
                    <Route exact path='/login' element={<Login />} />
                </Routes>
            </div>
        </Router>
    );
}
