import { useSearchParams, useLocation } from "react-router-dom"

const Detail = () => {


  const location = useLocation()
  const goodsNo = location.state.goodsNo

  const [searchParams] = useSearchParams()
  let id = searchParams.getAll('id')[0]
  return (
    <>
      <div>Detail页面--- id：{id}</div>
      <div>Detail页面事件跳转取参--- goodsNo：{goodsNo}</div>
    </>
  )
}

export default Detail;